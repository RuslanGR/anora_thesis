from django.db import models


class Event(models.Model):

    EVENT_TYPE_CHOICES = (
        ('0', 'Конференции'),
        ('1', 'Монографии'),
        ('2', 'борники статей'),
    )
    EVENT_TOPIC_CHOICES = (
        ('0', 'Архиология'),
        ('1', 'Архитектура, Строительство'),
    )
    EVENT_FORM_CHOICES = (
        ('0', 'Заочная'),
        ('1', 'Очная'),
        ('2', 'Очно-заочная'),
    )

    name = models.CharField(max_length=255, verbose_name='Название мероприятия')
    event_type = models.CharField(max_length=30, choices=EVENT_TYPE_CHOICES, verbose_name='Тематика')
    topic = models.CharField(max_length=255, choices=EVENT_TOPIC_CHOICES, verbose_name='Тип мероприятия')
    event_form = models.CharField(max_length=30, choices=EVENT_FORM_CHOICES, verbose_name='Форма участия')

    start_date = models.DateTimeField(verbose_name='Дата начала')
    end_date = models.DateTimeField(verbose_name='Дата окончания')

    description = models.TextField(verbose_name='Подробное описание')
    deadline = models.DateTimeField(verbose_name='Срок подачи заявок')

    organizers = models.TextField(verbose_name='Организаторы')
    contact_info = models.TextField(verbose_name='Контактная информация', default='')
    email = models.EmailField(verbose_name='Адрес электронной почты')
    country = models.CharField(max_length=30, verbose_name='Страна', default='')
    city = models.CharField(max_length=30, verbose_name='Город', default='')
