from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Event
from .forms import EventForm


class EventView(View):
    def get(self, request, pk):
        event = Event.objects.get(pk=pk)
        return render(request, 'central/event.html', {'event': event})


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('central:index')


class LoginView(View):
    def get(self, request):
        return render(request, 'central/login.html')

    def post(self, request):

        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
        else:
            print('lol')
        print(user)
        return render(request, 'central/login.html')


class IndexView(View):
    def get(self, request):
        events = Event.objects.all()
        return render(request, 'central/index.html', {'events': events})


class ReferenceView(View):
    def get(self, request):
        events = Event.objects.all()
        return render(request, 'central/reference.html', {'events': events})


class ArchiveView(View):
    def get(self, request):
        events = Event.objects.all()
        return render(request, 'central/archive.html', {'events': events})


class CreateView(LoginRequiredMixin, View):
    def get(self, request):
        form = EventForm()
        return render(request, 'central/create.html', {'form': form})

    def post(self, request):
        form = EventForm(request.POST)
        print(request.POST)
        if form.is_valid():
            form.save()
        else:
            print(form.errors)

        return render(request, 'central/create.html', {'form': EventForm()})
