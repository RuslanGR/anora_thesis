from django.urls import path

from .views import (
    IndexView,
    CreateView,
    EventView,
    ArchiveView,
    ReferenceView,

    LoginView,
    LogoutView,
)


app_name = 'central'
urlpatterns = (
    path('', IndexView.as_view(), name='index'),
    path('create/', CreateView.as_view(), name='create'),
    path('event/<int:pk>/', EventView.as_view(), name='event'),
    path('archive/', ArchiveView.as_view(), name='archive'),
    path('reference/', ReferenceView.as_view(), name='reference'),

    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
)
