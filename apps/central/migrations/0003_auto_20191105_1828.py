# Generated by Django 2.2.7 on 2019-11-05 15:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('central', '0002_auto_20191105_1503'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='city',
            field=models.CharField(default='', max_length=30, verbose_name='Город'),
        ),
        migrations.AddField(
            model_name='event',
            name='country',
            field=models.CharField(default='', max_length=30, verbose_name='Страна'),
        ),
    ]
