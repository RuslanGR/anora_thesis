from django.forms import ModelForm, DateInput, TextInput, Select, Textarea, EmailInput

from .models import Event


class DateInputWidget(DateInput):
    input_type = 'date'


class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = '__all__'
        widgets = {
            'start_date': DateInputWidget(attrs={'class': 'form-control'}),
            'end_date': DateInputWidget(attrs={'class': 'form-control'}),
            'deadline': DateInputWidget(attrs={'class': 'form-control'}),
            'name': TextInput(attrs={'class': 'form-control'}),
            'event_type': Select(attrs={'class': 'form-control'}),
            'topic': Select(attrs={'class': 'form-control'}),
            'event_form': Select(attrs={'class': 'form-control'}),
            'city': TextInput(attrs={'class': 'form-control'}),
            'country': TextInput(attrs={'class': 'form-control'}),
            'description': Textarea(attrs={'class': 'form-control', 'rows': 5}),
            'organizers': Textarea(attrs={'class': 'form-control', 'rows': 3}),
            'contact_info': Textarea(attrs={'class': 'form-control', 'rows': 3}),
            'email': EmailInput(attrs={'class': 'form-control'}),
        }
